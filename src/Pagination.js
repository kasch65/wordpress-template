import React from 'react'
import './Pagination.css'


const Pagination = ({ page, pages, pageSelectHandler }) => {

	const buttons = []
	for (let i = 1; i <= pages; ++i) {
		buttons.push(i)
	}
	return (
		<div id='pagination'>Seite:
			{
				buttons.map((value, key) => <button
					key={key}
					value={value}
					onClick={() => pageSelectHandler(value)}
					className={value === page ? 'active' : ''}>{value}</button>
				)
			}
		</div>
	)
}

export default Pagination
