import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Toolbar from '@material-ui/core/Toolbar'
import Link from '@material-ui/core/Link'

//const anchorRef = React.useRef(null)

const useStyles = makeStyles(theme => {
	console.debug('Theme: ', theme)
	return {
		toolbar: {
			borderBottom: `1px solid ${theme.palette.divider}`,
		},
		toolbarTitle: {
			flex: 1,
		},
		toolbarPrimary: {
			justifyContent: 'space-between',
			overflowX: 'auto',
		},
		toolbarSecondary: {
			borderTop: `1px solid ${theme.palette.divider}`,
			justifyContent: 'center',
			overflowX: 'auto',
		},
		toolbarLink: {
			padding: theme.spacing(1),
			flexShrink: 0,
		},
		toolbarSecondaryLink: {
			paddingRight: '3rem',
		},
		toolbarActiveLink: {
			textDecoration: 'underline',
		}
	}
})

const Nav = ({ menu, selectHandler, crurrentPrimaryItem, crurrentSecondaryItem }) => {
	const classes = useStyles()

	return (
		<>
			<Toolbar component="nav" variant="dense" className={classes.toolbarPrimary}>
				{
					menu.data.map((item) =>
						<Link
							color="inherit"
							noWrap
							variant="body2"
							key={item.id}
							id={(item.object === 'category' ? 'c-' : 'p-') + item.object_id}
							className={classes.toolbarLink + ' ' + (crurrentPrimaryItem === item ? classes.toolbarActiveLink : '')}
							onClick={() => selectHandler(item, 1)}>
							{item.object === 'category' ? 'K' : 'S'}: {item.title}
						</Link>
					)
				}
			</Toolbar>

			{
				crurrentPrimaryItem &&
				crurrentPrimaryItem.child_items &&
				<Toolbar component="nav" variant="dense" className={classes.toolbarSecondary}>
					{
						crurrentPrimaryItem.child_items.map((item) =>
							<Link
								color="inherit"
								noWrap
								variant="body2"
								key={item.id}
								id={(item.object === 'category' ? 'c-' : 'p-') + item.object_id}
								className={classes.toolbarSecondaryLink + ' ' + (crurrentSecondaryItem === item ? classes.toolbarActiveLink : '')}
								onClick={() => selectHandler(item, 2)}>
								{item.object === 'category' ? 'K' : 'S'}: {item.title}
							</Link>
						)
					}
				</Toolbar>
			}
		</>
	)
}

export default Nav
