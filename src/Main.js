import React from 'react'
import Page from './Page'
import Posts from './Posts'
import Loader from './Loader'

const Main = ({ categoryInfo, posts, page, onPageSelect }) => {
	return (
		<main>
			{
				!page && !categoryInfo &&
				<Loader />
			}
			{
				page &&
				<Page page={page} />
			}
			{
				(categoryInfo || posts) &&
				<Posts categoryInfo={categoryInfo} posts={posts} onPageSelect={onPageSelect} />
			}
			{
				categoryInfo && !posts &&
				<Loader />
			}
		</main>
	)
}

export default Main