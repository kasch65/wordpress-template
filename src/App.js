import React, { useState, useEffect } from 'react'
import wpMenuService from './services/wpMenue'
import wpPageService from './services/wpPages'
import wpCategoryService from './services/wpCategories'
import CssBaseline from '@material-ui/core/CssBaseline'
import Container from '@material-ui/core/Container'

//import './App.css'
import './WP.css'
import Header from './Header'
import Main from './Main'

const App = () => {
	const [homeItem, setHomeItem] = useState(null)
	const [menu, setMenu] = useState(null)
	const [crurrentPrimaryItem, setCrurrentPrimaryItem] = useState(null)
	const [crurrentSecondaryItem, setCrurrentSecondaryItem] = useState(null)
	const [categoryInfo, setCategoryInfo] = useState(null)
	const [posts, setPosts] = useState(null)
	const [page, setPage] = useState(null)

	useEffect(() => {
		wpMenuService.getMenue(window.globalWpConfig.menuName)
			.then(m => {
				console.debug('Menu: ', m)
				setMenu(() => {
					_onNavigation(m.data[0], 0, 1)
					setTimeout(() => _precacheAll(m), 4000)
					return m
				})
			})
	}, [])

	const _onPageSelect = page => {
		const menuItem = crurrentSecondaryItem || crurrentPrimaryItem
		if (menuItem) {
			setPosts(null)
			wpCategoryService.getPosts(menuItem.object_id, page, 10)
				.then(p => setPosts(p))
		}
	}

	const _precacheAll = m => {
		console.debug('Precaching pages referenced in menu...')
		if (window.cachedAll) {
			return
		}
		window.cachedAll = true
		window.map = new Map()
		m.data.forEach(item => {
			window.map.set(item.object + '_' + item.object_id, item)
			if (item.child_items) {
				item.child_items.forEach(item2 => {
					window.map.set(item2.object + '_' + item2.object_id, item2)
				})
			}
		})
		let first = true
		window.map.forEach(item => {
			if (first) {
				// First page gets loaded as startpage already -> skip it!
				first = false
			}
			else {
				if (item.object === 'category') {
					wpCategoryService.getCatInfo(item.object_id)
					wpCategoryService.getPosts(item.object_id, 1, 10)
						.then(i => {
							for (let p = 2; p <= i.pages; ++p) {
								wpCategoryService.getPosts(item.object_id, p, 10)
							}
						})
				}
				if (item.object === 'page') {
					wpPageService.getPage(item.object_id)
				}
			}
		})
	}

	const _onNavigation = (item, menuLevel, page) => {
		if (menuLevel === 0) {
			setHomeItem(item)
			setCrurrentPrimaryItem(item)
			setCrurrentSecondaryItem(null)
		}
		if (menuLevel === 1) {
			setCrurrentPrimaryItem(item)
			setCrurrentSecondaryItem(null)
		}
		else if (menuLevel === 2) {
			setCrurrentSecondaryItem(item)
		}
		if (item.object === 'category') {
			setPage(null)
			const categoryPromises = []
			// Category change?
			const categoryChange = (!categoryInfo || categoryInfo.id !== item.object_id)
			if (categoryChange) {
				categoryPromises.push(wpCategoryService.getCatInfo(item.object_id))
			}
			categoryPromises.push(wpCategoryService.getPosts(item.object_id, categoryChange ? 1 : page ? page : 1, 10))
			Promise.all(categoryPromises)
				.then(res => {
					if (res.length === 1) {
						setCategoryInfo(res[0])
					}
					if (res.length === 2) {
						setCategoryInfo(res[0])
						setPosts(res[1])
					}
				})
		}
		else if (item.object === 'page') {
			setCategoryInfo(null)
			setPosts(null)
			wpPageService.getPage(item.object_id)
				.then(data => setPage(data))
		}
	}

	return (
		<React.Fragment>
			<CssBaseline />
			<Container maxWidth="lg">
				<Header menu={menu} selectHandler={(item, menuLevel) => _onNavigation(item, menuLevel)} crurrentPrimaryItem={crurrentPrimaryItem} crurrentSecondaryItem={crurrentSecondaryItem} homeItem={homeItem} />
				<Main categoryInfo={categoryInfo} posts={posts} page={page} onPageSelect={p => _onPageSelect(p)} />
				<footer>
					<div>Künstlerhaus AUSSPANN</div>
				</footer>
			</Container>
		</React.Fragment>
	)
}

export default App