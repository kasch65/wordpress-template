import pouchBasics from './pouchBasics'
import PouchDB from 'pouchdb-browser'
const dbName = 'wp-cache-ausspann'
const localDb = new PouchDB(dbName, { auto_compaction: true })

const addItem = item => pouchBasics.addItem(localDb, item)

const getItem = id => pouchBasics.getItem(localDb, id)

const getCatInfo = id => {
	return new Promise((resolve, reject) => {
		getItem('ausspann-categories-' + id)
			.then(pouchItem => {
				if (pouchItem) {
					resolve(pouchItem)
				}
			})
			.catch(() => {
				console.debug('Category not cached yet: ', id)
				fetch(`/api/wp-json/wp/v2/categories/${id}?_fields=name,description`)
					.then(response => response.json()
						.then(data => {
							const result = { id, name: data.name, description: data.description }
							result._id = 'ausspann-categories-' + id
							addItem(result)
								.then(saved => resolve(saved))
								.catch(() => console.debug('Category already cached: ', id))
						})
					)
					.catch(err => reject(err))
			})
	})
}

const getPosts = (catId, page, perPage) => {
	return new Promise((resolve, reject) => {
		getItem('ausspann-posts-' + catId + '-' + page)
			.then(pouchItem => {
				if (pouchItem) {
					resolve(pouchItem)
				}
			})
			.catch(() => {
				console.debug('Posts not cached yet: ', catId, page)
				fetch(`${window.globalWpConfig.restBaseUrl}/wp-json/wp/v2/posts?categories=${catId}&_fields=id,categories,title,excerpt,content&orderby=date&order=desc&page=${page}&per_page=${perPage}`)
					.then(response => {
						let pages = 1
						for (var pair of response.headers.entries()) {
							if (pair[0] === 'x-wp-totalpages') {
								pages = pair[1]
							}
						}
						response.json().then(data => {
							const result = { data, page, pages: Number(pages), perPage }
							result._id = 'ausspann-posts-' + catId + '-' + page
							addItem(result)
								.then(saved => resolve(saved))
								.catch(() => console.debug('Post already cached: ', catId, page))
						})
					})
					.catch(err => reject(err))
			})
	})
}

export default { getCatInfo, getPosts }