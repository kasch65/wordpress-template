const sort = (sorters, array) => {
	let tmp = array
	sorters.forEach(sorter => {
		tmp = tmp.sort(sorter)
	})
	return tmp
}

const addItem = (db, item) => {
	const clone = { ...item }
	delete clone.id
	delete clone.rev
	return new Promise((resolve, reject) => {
		db.post(clone)
			.then(result => {
				clone.id = result.id
				clone.rev = result.rev
				resolve(clone)
			})
			.catch(err => reject(err))
	})
}

const getItems = (db, sorters) => {
	return new Promise((resolve, reject) => {
		db.allDocs({ include_docs: true, descending: true })
			.then(result => {
				const objects = sort(sorters, result.rows
					.map(row => {
						const doc = row.doc
						doc.id = doc._id
						delete doc._id
						doc.rev = doc._rev
						delete doc._rev
						return doc
					}))
				resolve(objects)
			})
			.catch(err => reject(err))
	})
}

const getItem = (db, id) => {
	return new Promise((resolve, reject) => {
		db.get(id)
			.then(result => {
				result.id = result._id
				delete result._id
				result.rev = result._rev
				delete result._rev
				return resolve(result)
			})
			.catch(err => reject(err))
	})
}

const patchItem = (db, id, patch) => {
	return new Promise((resolve, reject) => {
		db.get(id)
			.then(old => {
				const clone = { ...old, ...patch }
				db.put(clone)
					.then(result => {
						clone.id = result.id
						delete clone._id
						clone.rev = result.rev
						delete clone._rev
						return resolve(clone)
					})
					.catch(err => reject(err))
			})
			.catch(err => reject(err))
	})
}

const deleteItem = (db, id) => {
	return new Promise((resolve, reject) => {
		db.get(id)
			.then(old => {
				db.remove(old._id, old._rev)
					.then(result => {
						old.id = result.id
						delete old._id
						old.rev = result.rev
						delete old._rev
						return resolve(old)
					})
					.catch(err => reject(err))
			})
			.catch(err => reject(err))
	})
}

const onChange = (db, sorters, setItems, onAddItem, onPatchItem, onDeleteItem) => {
	db.changes({
		since: 'now',
		live: true,
		include_docs: true
	})
		.on('change', change => {
			if (!change.deleted) {
				const item = change.doc
				item.id = item._id
				delete item._id
				item.rev = item._rev
				delete item._rev
				const itemRevParts = item.rev.split('-')
				const itemRev = Number(itemRevParts[0])
				if (setItems) {
					setItems(b => {
						// Remove old item if exists
						let clone
						const oldItem = b.find(c => c.id === item.id)
						if (itemRev === 1) {
							// new item
							console.log('onNew: ', item)
							if (oldItem) {
								console.warn('Unexpected old item: ', oldItem.id, oldItem.rev)
								clone = b.filter(c => c.id !== item.id)
								// Add new or changed item
								clone.push(item)
							}
							else {
								clone = b.concat(item)
							}
							clone = sort(sorters, clone)
						}
						else if (itemRev > 1) {
							// Patch
							console.log('onPatch: ', item)
							if (!oldItem) {
								console.warn('Missing old item to update with. Adding instead: ', item.id, item.rev)
								// Add new item
								clone = b.concat(item)
								clone = sort(sorters, clone)
							}
							else {
								console.debug('Old and new revision: ', oldItem.rev, item.rev)
								const oldRevParts = oldItem.rev.split('-')
								if (Number(itemRevParts[0]) <= Number(oldRevParts[0])) {
									console.warn('A conflict has happened! A remote client has changed the item in the meantime!', oldItem.rev, item.rev)
								}
								clone = b.filter(c => c.id !== item.id)
								// Update instead of replace item, no new sort required
								Object.keys(oldItem).forEach(key => delete oldItem[key])
								Object.assign(oldItem, item)
								return [...b]
							}
						}
						// Return sorted list
						return clone
					})
				}
				if (onAddItem && itemRev === 1) {
					onAddItem(item)
				}
				if (onPatchItem && itemRev > 1) {
					onPatchItem(item)
				}
			}
			else if (change.deleted) {
				if (setItems) {
					console.log('onDelete: ', change.id)
					setItems(b => {
						// Remove old item if exists
						const oldItem = b.find(c => c.id === change.id)
						if (oldItem) {
							console.debug('Old and new revision after delete: ', oldItem.rev, change.doc._rev)
							const oldRevParts = oldItem.rev.split('-')
							const newRevParts = change.doc._rev.split('-')
							if (Number(newRevParts[0]) <= Number(oldRevParts[0])) {
								console.warn('A conflict has happened! A remote client has changed the item in the meantime!', oldItem.rev, change.doc._rev)
							}
							return b.filter(c => c.id !== change.id)
						}
						else {
							console.warn('Missing old item to delete: ', change.id)
							return b
						}
					})
				}
				if (onDeleteItem) {
					onDeleteItem(change.id)
				}
			}
		})
}

const sync = (localDb, remoteDb) => {
	// console.log('replication with ', remoteCouch)
	var opts = {
		live: true,
		retry: true
	}
	const syncHandler = localDb.sync(remoteDb, opts)
	syncHandler
		.on('change', change => console.debug('Local or remote DB has changed.', localDb.name, change))
		.on('paused', () => console.debug('Local and remote DB paused.', localDb.name))
		.on('active', info => console.debug('Local and remote DB sync resumed.', localDb.name, info))
		.on('complete', info => console.debug('Local and remote DB sync canceled.', localDb.name, info))
		.on('error', error => console.debug('Local and remote DB sync error.', localDb.name, error))
	//toRep = localDb.replicate.to(remoteDb, opts, syncError => console.log(syncError))
	//fromRep = localDb.replicate.from(remoteDb, opts, syncError => console.log(syncError))
	syncHandler.localDb = localDb
	return syncHandler
}

const stop = syncHandler => {
	if (syncHandler) {
		if (syncHandler.localDb) {
			syncHandler.localDb.viewCleanup()
				.then(() => {
					console.debug('DB view cleanup successful', syncHandler.localDb.name)
					console.debug('stopping replication from ', syncHandler.localDb.name, syncHandler)
					syncHandler.cancel()
				})
				.catch(err => {
					console.warn(err)
				})
		}
	}
}

export default { addItem, getItems, getItem, patchItem, deleteItem, onChange, sync, stop }