import pouchBasics from './pouchBasics'
import PouchDB from 'pouchdb-browser'
const dbName = 'wp-cache-ausspann'
const localDb = new PouchDB(dbName, { auto_compaction: true })

const addItem = item => pouchBasics.addItem(localDb, item)

const getItem = id => pouchBasics.getItem(localDb, id)

// Required Plugin: https://wordpress.org/plugins/wp-rest-api-v2-menus/
const getMenue = name => {
	return new Promise((resolve, reject) => {
		getItem('ausspann-menu-' + name)
			.then(pouchItem => {
				if (pouchItem) {
					resolve(pouchItem)
				}
			})
			.catch(() => {
				console.debug('Menu not cached yet: ', name)
				fetch(`/api/wp-json/menus/v1/menus/${name}`)
					.then(response => {
						response.json()
							.then(data => {
								if (!data.items) {
									return
								}
								// Sort menu items
								data.items.sort((a, b) => { return a.menu_order - b.menu_order })
								// Sort sub menu items
								data.items.forEach(item => {
									if (item.child_items) {
										item.child_items.sort((a, b) => { return a.menu_order - b.menu_order })
									}
								})
								const result = {
									name, data: data.items
										.sort((a, b) => { return a.menu_order - b.menu_order })
										.map(m => {
											const item = {
												id: m.ID,
												object: m.object,
												object_id: m.object_id,
												title: m.title
											}
											if (m.child_items) {
												item.child_items = m.child_items
													.sort((a, b) => { return a.menu_order - b.menu_order })
													.map(m2 => {
														return {
															id: m2.ID,
															object: m2.object,
															object_id: m2.object_id,
															title: m2.title
														}
													})
											}
											return item
										})
								}
								result._id = 'ausspann-menu-' + name
								addItem(result)
									.then(saved => resolve(saved))
									.catch(() => console.debug('Menue already cached: ', name))
							})
					})
					.catch(err => reject(err))

			})
	})
}

export default { getMenue }