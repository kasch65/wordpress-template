import pouchBasics from './pouchBasics'
import PouchDB from 'pouchdb-browser'
const dbName = 'wp-cache-ausspann'
const localDb = new PouchDB(dbName, { auto_compaction: true })

const addItem = item => pouchBasics.addItem(localDb, item)

const getItem = id => pouchBasics.getItem(localDb, id)

const getPage = id => {
	return new Promise((resolve, reject) => {
		getItem('ausspann-pages-' + id)
			.then(pouchItem => {
				if (pouchItem) {
					resolve(pouchItem)
				}
			})
			.catch(() => {
				console.debug('Page not cached yet: ', id)
				fetch(`/api/wp-json/wp/v2/pages/${id}?_fields=id,title,content,_links`)
					.then(response => {
						response.json().then(data => {
							let mediaUrl
							if (data._links['wp:featuredmedia'] && data._links['wp:featuredmedia'][0].href) {
								mediaUrl = data._links['wp:featuredmedia'][0].href
							}
							if (mediaUrl) {
								console.debug('Fetching: ', mediaUrl)
								fetch(mediaUrl)
									.then(mediaResponse => {
										mediaResponse.json().then(mediaData => {
											console.debug(`Page # ${id} with image ${mediaData.media_details.sizes.full.source_url} loaded.`)
											const result = { id, data, imageUrl: mediaData.media_details.sizes.full.source_url }
											result._id = 'ausspann-pages-' + id
											addItem(result)
												.then(saved => resolve(saved))
												.catch(() => console.debug('Page already cached: ', id))
										})
									})
							}
							else {
								console.debug(`Page # ${id} loaded.`)
								const result = { id, data }
								result._id = 'ausspann-pages-' + id
								addItem(result)
									.then(saved => resolve(saved))
									.catch(() => console.debug('Page already cached: ', id))
							}
						})
					})
					.catch(err => reject(err))
			})
	})
}

export default { getPage }