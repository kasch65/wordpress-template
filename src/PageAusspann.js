import React from 'react'
import Page from './Page'
import ReactHtmlParser from 'react-html-parser'

class PageAusspann extends Page {
	
	/**
	 * Override content renderer
	 */
	renderContent() {
		return this.state.data.map(
			(item, key) =>
				<section key={key} id={'page_' + this.props.menuItem} className='page-ausspann'>
					<h1>{item.title.rendered}</h1>
					<section>{ReactHtmlParser(item.content.rendered)}</section>
				</section>
		)
	}

}

export default PageAusspann