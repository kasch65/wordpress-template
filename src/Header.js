import React, { useState } from 'react'
import Toolbar from '@material-ui/core/Toolbar'
import { makeStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import Nav from './Nav'
import Loader from './Loader'

const useStyles = makeStyles(theme => {
	console.debug('Theme: ', theme)
	return {
		toolbar: {
			borderBottom: `1px solid ${theme.palette.divider}`,
		},
		toolbarTitle: {
			flex: 1,
		},
	}
})

const Header = ({ menu, selectHandler, crurrentPrimaryItem, crurrentSecondaryItem, homeItem }) => {
	const classes = useStyles()
	const [pageName, setPageName] = useState(null)
	const [pageDescription, setPageDescription] = useState(null)
	const [loaded, setLoaded] = useState(false)
	let _loading = 0

	if (!pageName) {
		_loading = 1
		fetch(`${window.globalWpConfig.restBaseUrl}/wp-json`)
			.then(response => {
				response.json()
					.then(data => {
						--_loading
						setPageName(data.name)
						setPageDescription(data.description)
						setLoaded(_loading === 0)
					})
			})
	}

	return (
		<React.Fragment>
			<header>
				{
					loaded &&
					<Toolbar className={classes.toolbar}>
						<div onClick={() => selectHandler(homeItem, 1)}>
							<img src={`${window.globalWpConfig.restBaseUrl}/wp-content/uploads/2016/10/Ausspann_Logo_Web.png`} alt="Logo"></img>
						</div>
						<Typography
							component="h2"
							variant="h5"
							color="inherit"
							align="center"
							noWrap
							className={classes.toolbarTitle}
						>{pageName}</Typography>
						<div>{pageDescription}</div>
					</Toolbar>
				}
				{
					!menu &&
					<Loader />
				}
				{
					menu &&
					<Nav menu={menu} selectHandler={selectHandler} crurrentPrimaryItem={crurrentPrimaryItem} crurrentSecondaryItem={crurrentSecondaryItem} />
				}
			</header>
		</React.Fragment>
	)

}

export default Header
