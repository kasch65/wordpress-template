import React from 'react'
import loaderGif from './img/ajax-loader.gif'

const Loader = () => {
	return <div className="loader"><img src={loaderGif} alt="Loading" /></div>
}

export default Loader