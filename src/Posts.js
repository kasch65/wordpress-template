import React from 'react'
import ReactHtmlParser from 'react-html-parser'
import { makeStyles } from '@material-ui/core/styles'
import Pagination from './Pagination'
import jQuery from 'jquery'
import Paper from '@material-ui/core/Paper'

const useStyles = makeStyles(theme => {
	console.debug('Theme: ', theme)
	return {
		paper: {
			padding: '1rem',
			marginBottom: '1rem'
		}
	}
})

const Posts = ({ categoryInfo, posts, onPageSelect }) => {
	const classes = useStyles()

	const _addMetaslider = () => {
		var metaslider_36 = function ($) {
			$('#metaslider_36').addClass('flexslider')
			$('#metaslider_36').flexslider({
				slideshowSpeed: 6000,
				animation: 'fade',
				controlNav: false,
				directionNav: true,
				pauseOnHover: false,
				direction: 'horizontal',
				reverse: false,
				animationSpeed: 2500,
				prevText: '&lt',
				nextText: '&gt',
				fadeFirstSlide: true,
				slideshow: true
			})
			$(document).trigger('metaslider/initialized', '#metaslider_36')
		}
		var timer_metaslider_36 = function () {
			!window.jQuery ? window.setTimeout(timer_metaslider_36, 100) : !jQuery.isReady ? window.setTimeout(timer_metaslider_36, 1) : metaslider_36(window.jQuery)
		}
		timer_metaslider_36()
	}

	_addMetaslider()

	return (
		<section id={'category_' + categoryInfo.id}>
			<Paper className={classes.paper}>
				<h1>{categoryInfo.name}</h1>
				<summary>{categoryInfo.description}</summary>
				<Pagination
					pages={posts ? posts.pages : 1}
					page={posts ? posts.page : 1}
					pageSelectHandler={p => onPageSelect(p)} />
			</Paper>
			{
				posts &&
				<>
					{
						posts.data.map(
							item =>
								<article key={item.id}>
									<Paper className={classes.paper}>
										<h3>{item.title.rendered}</h3>
										<summary>{ReactHtmlParser(item.excerpt.rendered)}</summary>
										<details>{ReactHtmlParser(item.content.rendered)}</details>
									</Paper>
								</article>
						)
					}
				</>
			}
		</section>
	)

}

export default Posts