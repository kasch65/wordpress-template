import React from 'react'
import ReactHtmlParser from 'react-html-parser'
import { makeStyles } from '@material-ui/core/styles'
import jQuery from 'jquery'
import Paper from '@material-ui/core/Paper'

const useStyles = makeStyles(theme => {
	console.debug('Theme: ', theme)
	return {
		paper: {
			padding: '1rem',
			marginBottom: '1rem'
		}
	}
})

const Page = ({ page }) => {
	const classes = useStyles()

	const _addMetaslider = () => {
		var metaslider_36 = function ($) {
			$('.metaslider>div>div').addClass('flexslider')
			$('.metaslider>div>div').flexslider({
				slideshowSpeed: 6000,
				animation: 'fade',
				controlNav: false,
				directionNav: true,
				pauseOnHover: false,
				direction: 'horizontal',
				reverse: false,
				animationSpeed: 2500,
				prevText: '&lt',
				nextText: '&gt',
				fadeFirstSlide: true,
				slideshow: true
			})
			$(document).trigger('metaslider/initialized', '.metaslider>div>div')
		}
		var timer_metaslider_36 = function () {
			!window.jQuery ? window.setTimeout(timer_metaslider_36, 100) : !jQuery.isReady ? window.setTimeout(timer_metaslider_36, 1) : metaslider_36(window.jQuery)
		}
		timer_metaslider_36()
	}

	setTimeout(() => _addMetaslider(), 1)

	return <section id={'page_' + page.id}>
		<Paper className={classes.paper}>
			{
				page.imageUrl &&
				<img src={page.imageUrl} alt='PageImage'></img>
			}
			<h1>{page.data.title.rendered}</h1>
			<section>{ReactHtmlParser(page.data.content.rendered)}</section>
		</Paper>
	</section>
}

export default Page