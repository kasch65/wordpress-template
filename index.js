require('dotenv').config()
const express = require('express')
const proxy = require('http-proxy-middleware')
const morgan = require('morgan')
const compression = require('compression')
const modifyResponse = require('node-http-proxy-json')

console.log('Starting server...')

const app = express()

// compress all responses
app.use(compression())

//app.use(morgan('tiny'))
app.use(morgan((tokens, req, res) => {
	return [
		tokens.method(req, res),
		tokens.url(req, res),
		tokens.status(req, res),
		tokens.res(req, res, 'content-length'), '-',
		tokens['response-time'](req, res), 'ms',
		JSON.stringify(req.body)
	].join(' ')
}))

// Redirect http to https
app.use((req, res, next) => {
	if (req.header('x-forwarded-proto') && req.header('x-forwarded-proto') !== 'https') {
		const newLocation = `https://${req.header('host')}${req.url}`
		console.debug('redirecting to: ', newLocation)
		res.redirect(newLocation)
	} else {
		next()
	}
})

app.use(express.static('build'))

const _replaceUrl = input => {
	//console.log('Changin content: ', input)
	return input.replace(/http:\/\/ausspann-bremen\.de/gi, '/api')
}

const _wpProxy = proxy({
	target: process.env.REST_URL,
	changeOrigin: true,
	//ws: true,
	//logLevel: 'debug',
	pathRewrite: {
		'^/api': '' // remove base path
	},
	onProxyRes: (proxyRes, req, res) => {
		//console.log(proxyRes)
		//proxyRes.headers['x-added'] = 'foobar' // add new header to response
		//delete proxyRes.headers['x-removed'] // remove header from response
		// modify some information
		//body.version = 42
		//body.props = {
		//	nestedProps: true
		//}
		const contentTypeHeader = proxyRes.headers['content-type']
		if (contentTypeHeader && contentTypeHeader.startsWith('application/json')) {
			//delete proxyRes.headers['content-length']
			modifyResponse(res, proxyRes, (body) => {
				if (body && body.content && body.content.rendered) {
					body.content.rendered = _replaceUrl(body.content.rendered)
				}
				if (body && body._links && body._links['wp:featuredmedia'] && body._links['wp:featuredmedia'].length && body._links['wp:featuredmedia'][0].href) {
					body._links['wp:featuredmedia'][0].href = _replaceUrl(body._links['wp:featuredmedia'][0].href)
				}
				if (body && body.media_details && body.media_details.sizes && body.media_details.sizes && body.media_details.sizes.full.source_url) {
					body.media_details.sizes.full.source_url = _replaceUrl(body.media_details.sizes.full.source_url)
				}
				if (body && body.length > 0) {
					body.forEach(part => {
						part.content.rendered = _replaceUrl(part.content.rendered)
					})
				}
				return body
			})
		}
	}
})

app.use('/api', _wpProxy)

// Auf ungültige URLs reagieren
const unknownEndpoint = (request, response) => {
	response.status(404).send({ error: 'unknown endpoint' })
}

app.use(unknownEndpoint)

// Fehler an client senden
const errorHandler = (error, request, response, next) => {
	console.error('Error: ', error.message)

	if (error.name === 'CastError' && error.kind === 'ObjectId') {
		return response.status(400).send({ error: 'malformatted id' })
	}
	next(error)
}

app.use(errorHandler)

const PORT = process.env.PORT || 3002
app.listen(PORT, () => {
	console.log(`Server running on port ${PORT}`)
})
